function fillListTotalBilledOutvoicesForPeriodIdItem( itemData, itemTemplate )
{
    var itemDataContent = itemData['READ_OUTVOICES_BILLEDFORPERIODID_DATA'];
    var dataBindings =
    {
        '|READ_OUTVOICES_BILLEDFORPERIODID_PERIOD_ID|': itemData['READ_OUTVOICES_BILLEDFORPERIODID_PERIOD_ID'],
        '|READ_OUTVOICES_BILLEDALLPERIODS_PERIOD_TYPE|': itemDataContent['READ_OUTVOICES_BILLEDALLPERIODS_PERIOD_TYPE'],
        '|READ_OUTVOICES_BILLEDFORPERIODID_AMOUNT_TOTAL|': controlResultOutVoicesBilledPeriods ( itemDataContent['READ_OUTVOICES_BILLEDFORPERIODID_AMOUNT_TOTAL'] ),
        '|READ_OUTVOICES_BILLEDFORPERIODID_USER_ID|': itemDataContent['READ_OUTVOICES_BILLEDFORPERIODID_USER_ID']
    };
    return replaceDataBindings(dataBindings, itemTemplate);
}

function readTotalBilledOutvoicesForPeriodId( userId, periodId, successCallback, failCallback, workingFNCallback )
{
    sendAjaxRequest
    (
        "GET",
        "app_logged_user_crud_outvoice_read_totalsBilled_periodId/readTotalsBilledOutVoicePeriodId",
        "userId="+userId+"&periodId="+periodId,
        successCallback,
        failCallback,
        workingFNCallback
    );
}

function controlResultOutVoicesBilledPeriods ( RESULT )
{
    if( RESULT == null)
    {
        RESULT = "0";
    }
    return(RESULT);
}