function addOutvoice(userId, id_period, date, amount, successCallback, failCallback, workingFNCallback)
{
    sendAjaxRequest
    (
        "GET",
        "app_outvoice_crud_create/create_outvoice",
        "userId="+userId+"&periodId="+id_period+"&date="+date+"&amount="
        +amount,
        successCallback,
        failCallback,
        workingFNCallback
    );
}