function fillTotalBilledInvoicesForPeriodIdItem( itemData, itemTemplate )
{
    var itemDataContent = itemData['READ_INOUTVOICES_TOTALSLOADEDS_DIFFERENCE_PERIODID_DATA'];
    var dataBindings =
    {   
        '|READ_INOUTVOICES_BILLEDFORPERIODID_PERIOD_ID|': itemData['READ_INOUTVOICES_BILLEDFORPERIODID_PERIOD_ID'],
        '|READ_INOUTVOICES_TOTALSLOADEDS_DIFFERENCE_PERIODID_PERIOD_TYPE|': itemDataContent['READ_INOUTVOICES_TOTALSLOADEDS_DIFFERENCE_PERIODID_PERIOD_TYPE'],
        '|READ_INOUTVOICES_TOTALSLOADEDS_DIFFERENCE_PERIODID_AMOUNT_TOTAL_INVOICE|': itemDataContent['READ_INOUTVOICES_TOTALSLOADEDS_DIFFERENCE_PERIODID_AMOUNT_TOTAL_INVOICE'],
        '|READ_INOUTVOICES_TOTALSLOADEDS_DIFFERENCE_PERIODID_AMOUNT_TOTAL_OUTVOICE|': itemDataContent['READ_INOUTVOICES_TOTALSLOADEDS_DIFFERENCE_PERIODID_AMOUNT_TOTAL_OUTVOICE'],
        '|READ_INOUTVOICES_TOTALSLOADEDS_DIFFERENCE_PERIODID_AMOUNT_TOTAL_DIFFERENCE|': itemDataContent['READ_INOUTVOICES_TOTALSLOADEDS_DIFFERENCE_PERIODID_AMOUNT_TOTAL_DIFFERENCE'],
        '|READ_INOUTVOICES_TOTALSLOADEDS_DIFFERENCE_PERIODID_USER_ID|': itemDataContent['READ_INOUTVOICES_TOTALSLOADEDS_DIFFERENCE_PERIODID_USER_ID']
    };
    return replaceDataBindings(dataBindings, itemTemplate);
}

function readTotalDifferenceInvoicesOutvoicesForPeriodId( userId, periodId, successCallback, failCallback, workingFNCallback )
{
    sendAjaxRequest
    (
        "GET",
        "app_logged_user_crud_inOutVoice_read_totalsLoadeds_difference_periodId/readTotalsLoadedsDifferencePeriodId",
        "userId="+userId+"&periodId="+periodId,
        successCallback,
        failCallback,
        workingFNCallback
    );
}