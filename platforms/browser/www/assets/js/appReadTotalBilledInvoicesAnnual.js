function fillListTotalBilledInvoicesAnnualListItem( itemData, itemTemplate )
{
    var itemDataContent = itemData['READ_INVOICES_BILLEDANNUALPERIODS_DATA'];
    var dataBindings =
    {   
        '|READ_INVOICES_BILLEDANNUALPERIODS_TOTAL_ANNUAL|': itemDataContent['READ_INVOICES_BILLEDANNUALPERIODS_TOTAL_ANNUAL'],
        '|READ_INVOICES_BILLEDANNUALPERIODS_LIMIT_BY_CATEGORY|': itemDataContent['READ_INVOICES_BILLEDANNUALPERIODS_LIMIT_BY_CATEGORY'],
        '|READ_INVOICES_BILLEDANNUALPERIODS_TOTAL_SURPASSED|': itemDataContent['READ_INVOICES_BILLEDANNUALPERIODS_TOTAL_SURPASSED']
    };
    return replaceDataBindings(dataBindings, itemTemplate);
}

function listTotalBilledInvoicesAnnual( userId, successCallback, failCallback, workingFNCallback )
{
    sendAjaxRequest
    (
        "GET",
        "app_logged_user_crud_invoice_read_totalsBilled_Annual/readTotalsBilledInVoiceAnnual",
        "userId="+userId,
        successCallback,
        failCallback,
        workingFNCallback
    );
}