function deleteOutvoiceById( userId, id_outvoice, successCallback, failCallback, workingFNCallback )
{
    sendAjaxRequest
    (
        "GET",
        "app_logged_user_crud_outvoice_delete/deleteOutvoice",
        "userId="+userId+"&outvoiceId="+id_outvoice,
        successCallback,
        failCallback,
        workingFNCallback
    );
}