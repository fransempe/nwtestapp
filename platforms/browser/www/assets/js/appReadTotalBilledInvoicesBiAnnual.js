function fillListTotalBilledInvoicesBiAnnualListItem( itemData, itemTemplate )
{
    var itemDataContent = itemData['READ_INVOICES_BILLEDBIANNUALPERIODS_DATA'];
    var dataBindings =
    {   
        '|READ_INVOICES_BILLEDBIANNUALPERIODS_TOTAL_BIANNUAL|': itemDataContent['READ_INVOICES_BILLEDBIANNUALPERIODS_TOTAL_BIANNUAL'],
        '|READ_INVOICES_BILLEDBIANNUALPERIODS_PARAMETER_BIANNUAL_RECATEGORIZATION|': itemDataContent['READ_INVOICES_BILLEDBIANNUALPERIODS_PARAMETER_BIANNUAL_RECATEGORIZATION'],
        '|READ_INVOICES_BILLEDBIANNUALPERIODS_TOTAL_SURPASSED|': itemDataContent['READ_INVOICES_BILLEDBIANNUALPERIODS_TOTAL_SURPASSED']
    };
    return replaceDataBindings(dataBindings, itemTemplate);
}

function listTotalBilledInvoicesBiAnnual( userId, successCallback, failCallback, workingFNCallback )
{
    sendAjaxRequest
    (
        "GET",
        "app_logged_user_crud_invoice_read_totalsBilled_BiAnnual/readTotalsBilledInVoiceBiAnnual",
        "userId="+userId,
        successCallback,
        failCallback,
        workingFNCallback
    );
}