function logOutApp(successCallback, workingFNCallback)
{
    sendAjaxRequest
    (
        "GET",
        "app_logout/logout",
        "",
        successCallback,
        "",
        workingFNCallback
    );
}

function logoutAppCallback( result_found, data )
{
   window.location = 'index.html';
}